package com;

import org.openqa.selenium.By;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.concurrent.TimeUnit;

public class Create_Account {
	public static void main(String[] args) {
		Create_Account create = new Create_Account();
		create.requestTempMail();
	}

	public static String email;
	public static String checkCode;

	public static String requestTempMail() {
		System.setProperty("webdriver.gecko.driver", "/home/dutv/Downloads/geckodriver");
		FirefoxDriver driver = new FirefoxDriver();
		driver.navigate().to("https://temp-mail.org/");
		driver.findElement(By.id("click-to-change")).click();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
		driver.findElement(By.cssSelector(".content input#mail")).sendKeys("dgdrftyr345");
		driver.findElement(By.cssSelector("#domain option:nth-child(7)")).click();
		driver.findElement(By.id("postbut")).click();

		WebElement els = driver.findElement(By.cssSelector(".yourmail input#mail"));
		email = els.getAttribute("value");
		driver.findElement(By.id("click-to-refresh")).click();

		// FB
		System.out.println("Start");
		System.setProperty("webdriver.gecko.driver", "/home/dutv/Downloads/geckodriver");
		String proxy = "195.154.162.201:6740";
		Proxy p = new Proxy();
		p.setHttpProxy(proxy).setFtpProxy(proxy).setSslProxy(proxy);

		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(CapabilityType.PROXY, p);

		WebDriver drivercap = new FirefoxDriver(cap);

		drivercap.navigate().to("https://facebook.com/");
		drivercap.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		drivercap.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		drivercap.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);

		// add field
		drivercap.findElement(By.cssSelector("input[name=lastname]")).sendKeys("Nguyen");
		drivercap.findElement(By.cssSelector("input[name=firstname]")).sendKeys("Chuc");
		drivercap.findElement(By.cssSelector("input[name=reg_email__]")).sendKeys(email);
		drivercap.findElement(By.cssSelector("input[name=reg_email_confirmation__]")).sendKeys(email);
		drivercap.findElement(By.cssSelector("input[name=reg_passwd__]")).sendKeys("master01231");
		drivercap.findElement(By.cssSelector("select[name=birthday_day] option:nth-child(6)")).click();
		drivercap.findElement(By.cssSelector("select[name=birthday_month] option:nth-child(3)")).click();
		drivercap.findElement(By.cssSelector("select[name=birthday_year] option:nth-child(33)")).click();
		drivercap.findElement(By.cssSelector("span[data-name=gender_wrapper] span:nth-child(2) input")).click();
		drivercap.findElement(By.cssSelector("button[name=websubmit]")).click();

		// get code
		String responeCode = driver.findElement(By.cssSelector(".content tbody tr td:nth-child(2) a")).getText();
		System.out.println(responeCode);
		checkCode = responeCode.substring(0, 5);
		System.out.println(checkCode);

		drivercap.findElement(By.id("code_in_cliff")).sendKeys(checkCode);
		System.out.println("before click confirm");
		drivercap.findElement(By.cssSelector(".clearfix button[name=confirm]")).click();
		drivercap.findElement(By.cssSelector(".topborder a")).click();

		driver.findElement(By.cssSelector(".clearfix a")).click();
		driver.findElement(By.cssSelector("div[class*=Footer] a::nth-child(1)")).click();
		// driver.findElement(By.cssSelector(".clearfix a")).click();
		System.out.println("Done");
		//

		return "";
	}

}
